import { createRouter, createWebHashHistory } from 'vue-router'
import App from './App.vue'

import LogIn from './components/LogIn.vue'
import SignUp from './components/SignUp.vue'
import Home from './components/Home.vue'
import Account from './components/Account.vue'
import Articulo from './components/Articulo.vue'
import RegistrarArticulo from './components/RegistrarArticulo.vue'
import ListarArticulo from './components/ListarArticulo.vue'
import ListaArticulos from './components/ListaArticulos.vue'
import RegistrarProveedor from './components/RegistrarProveedor.vue'

const routes = [
  {
    path: '/',
    name: 'root',
    component: App
  },

  {
    path: '/user/logIn',
    name: 'logIn',
    component: LogIn
               
  },

  {
    path: '/user/signUp',
    name: 'signUp',
    component: SignUp
  },

  {
    path: '/user/home',
    name: "home",
    component: Home
    },
  {
      path: '/user/account',
      name: "account",
      component: Account
  },
  /*{
    path: '/articulo/articulo',
    name: "articulo",
    component: Articulo
  },*/
  {
    path: "/articulo/articulo/:idarticulo",
    name: "articulo-details",
    component: Articulo
  },
  {
    path: '/articulo/registrarArticulo',
    name: "registrarArticulo",
    component: RegistrarArticulo
  },
  {
    path: '/articulo/listarArticulo',
    name: "listarArticulo",
    component: ListarArticulo
  },
  {
    path: '/articulo/listaArticulos',
    name: "listaArticulos",
    component: ListaArticulos
  },
  {
    path: '/proveedor/registrarProveedor',
    name: "registrarProveedor",
    component: RegistrarProveedor
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router;
