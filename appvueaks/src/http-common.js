import axios from "axios";

export default axios.create({
  baseURL: "https://alphakidsstore.herokuapp.com/",
  headers: {
    "Content-type": "application/json"
  }
});