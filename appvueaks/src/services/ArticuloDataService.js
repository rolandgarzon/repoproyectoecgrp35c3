import http from "../http-common";


class ArticuloDataService {
  getAll() {
    //return axios.get("/https://alphakidsstore.herokuapp.com/articulo/");
    return http.get("articulo/");
    
  }

  get(idarticulo) {
    console.log("Entro al axios")
    return http.get(`/articulo/detail/${idarticulo}`);
    //return http.get(`articulo/detail/1`);
  }

  create(data) {
    return http.post("/articulo", data);
  }

  update(idarticulo, data) {
    return http.put(`/articulo/update/${idarticulo}`, data);
    //return http.put(`articulo/update/1`, data);
  }

  delete(idarticulo) {
    return http.delete(`/articulo/delete/${idarticulo}`);
  }

  deleteAll() {
    return http.delete(`/articulo`);
  }

  findByTitle(referencia) {
    return http.get(`/articulo?referencia=${referencia}`);
  }
}

export default new ArticuloDataService();