from django.urls import path,include
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from appAlphaKidsStore.views import ArticuloListView
from appAlphaKidsStore import views
from django.conf import settings
from django.conf.urls.static import static 

from django import urls
from django.contrib import admin

urlpatterns = [
path('login/', TokenObtainPairView.as_view()),
path('refresh/', TokenRefreshView.as_view()),
path('user/', views.UserCreateView.as_view()),
path('user/<int:pk>/', views.UserDetailView.as_view()),
path('articulo/', include('appAlphaKidsStore.urls')),
path('proveedor/',include('appAlphaKidsStore.urls')),
]