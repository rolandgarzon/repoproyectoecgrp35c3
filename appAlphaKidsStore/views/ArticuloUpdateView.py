# Create your views here.
import json
from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from appAlphaKidsStore.models.articulo import Articulo
from appAlphaKidsStore.serializers.articuloSerializer import ArticuloSerializer
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse

@api_view(["PUT"])
#@csrf_exempt
#@permission_classes([IsAuthenticated])
def ArticuloUpdateView(request, pk):
    #user = request.user.id
    payload = json.loads(request.body)
    try:
        articulo_item = Articulo.objects.filter(idarticulo=pk)
        # returns 1 or 0
        articulo_item.update(**payload)
        articulo = Articulo.objects.get(idarticulo=pk)
        serializer = ArticuloSerializer(articulo)
        return JsonResponse({'articulo': serializer.data}, safe=False, status=status.HTTP_200_OK)
    except ObjectDoesNotExist as e:
        return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
    except Exception:
        return JsonResponse({'error': 'fuck Something terrible went wrong'}, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)