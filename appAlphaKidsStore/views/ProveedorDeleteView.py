from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from appAlphaKidsStore.models.proveedor import Proveedor
from appAlphaKidsStore.serializers.proveedorSerializer import ProveedorSerializer
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse

@api_view(["DELETE"])
#@csrf_exempt
#@permission_classes([IsAuthenticated])
def ProveedorDeleteView(request, pk):
    #user = request.user.id
    try:
        provee = Proveedor.objects.get(idproveedor=pk)
        provee.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    except ObjectDoesNotExist as e: 
        return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
    except Exception:
        return JsonResponse({'error': 'Something went wrong'}, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)