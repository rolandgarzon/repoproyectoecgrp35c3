# Create your views here.
from django.http.response import JsonResponse
from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from appAlphaKidsStore.models.articulo import Articulo
from appAlphaKidsStore.serializers.articuloSerializer import ArticuloSerializer


@api_view(['GET'])
def ArticuloDetailView(request, pk):
        articulo = Articulo.objects.get(idarticulo=pk) 
        if request.method == 'GET': 
            articuloSerializer = ArticuloSerializer(articulo) 
        return JsonResponse(articuloSerializer.data) 
    