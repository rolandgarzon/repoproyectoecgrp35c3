# Create your views here.
from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from appAlphaKidsStore.models.articulo import Articulo
from appAlphaKidsStore.serializers.articuloSerializer import ArticuloSerializer

@api_view(['GET', 'POST'])
def ArticuloListView(request):
    """
    Lista todos los articulos o permite crear uno nuevo
    """
    if request.method == 'GET':
        articulos = Articulo.objects.all()
        serializer = ArticuloSerializer(articulos, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = ArticuloSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)