# Create your views here.
from django.http.response import JsonResponse
from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from appAlphaKidsStore.models.proveedor import Proveedor
from appAlphaKidsStore.serializers.proveedorSerializer import ProveedorSerializer


@api_view(['GET'])
def ProveedorDetailView(request, pk):
        provee = Proveedor.objects.get(idproveedor=pk) 
        if request.method == 'GET': 
            proveedorSerializer = ProveedorSerializer(provee) 
        return JsonResponse(proveedorSerializer.data) 