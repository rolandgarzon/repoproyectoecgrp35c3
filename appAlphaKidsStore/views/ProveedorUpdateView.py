import json
from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from appAlphaKidsStore.models.proveedor import Proveedor
from appAlphaKidsStore.serializers.proveedorSerializer import ProveedorSerializer
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse

@api_view(["PUT"])
#@csrf_exempt
#@permission_classes([IsAuthenticated])
def ProveedorUpdateView(request, pk):
    #user = request.user.id
    payload = json.loads(request.body)
    try:
        proveedor_item = Proveedor.objects.filter(idproveedor=pk)
        # returns 1 or 0
        proveedor_item.update(**payload)
        provee = Proveedor.objects.get(idproveedor=pk)
        serializer = ProveedorSerializer(provee)
        return JsonResponse({'proveedor': serializer.data}, safe=False, status=status.HTTP_200_OK)
    except ObjectDoesNotExist as e:
        return JsonResponse({'error': str(e)}, safe=False, status=status.HTTP_404_NOT_FOUND)
    except Exception:
        return JsonResponse({'error': 'fuck Something terrible went wrong'}, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)