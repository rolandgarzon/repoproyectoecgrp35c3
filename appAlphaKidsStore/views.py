from django.shortcuts import render

# Create your views here.

from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView,UpdateView,DeleteView

from .models import Articulo
from .models import Color
from .models import Marca
from .models import Talla
from .models import TipoArticulo
from .models import Proveedor

from django.urls import reverse
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django import forms
