from django.contrib import admin
from django.urls import path,include

from appAlphaKidsStore.views.ArticuloListView import ArticuloListView
from appAlphaKidsStore.views.ArticuloDeleteView import ArticuloDeleteView
from appAlphaKidsStore.views.ArticuloUpdateView import ArticuloUpdateView
from appAlphaKidsStore.views.ArticuloDetailView import ArticuloDetailView
from appAlphaKidsStore.views.ProveedorListView import ProveedorListView
from appAlphaKidsStore.views.ProveedorDeleteView import ProveedorDeleteView
from appAlphaKidsStore.views.ProveedorDetailView import ProveedorDetailView
from appAlphaKidsStore.views.ProveedorUpdateView import ProveedorUpdateView

urlpatterns = [  
     path('', ArticuloListView),
     path('delete/<int:pk>', ArticuloDeleteView),
     path('update/<int:pk>', ArticuloUpdateView),
     path('detail/<int:pk>', ArticuloDetailView),
     path('provcreatelist', ProveedorListView),
     path('provdelete/<int:pk>', ProveedorDeleteView),
     path('provdetail/<int:pk>', ProveedorDetailView),
     path('provupdate/<int:pk>', ProveedorUpdateView),
     
     #'http://localhost:8000/articulo/update'
  
]