from django.db import models

class Color(models.Model):
    idcolor = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=500, default='default value')
    
    class Meta:
        db_table = 'color'