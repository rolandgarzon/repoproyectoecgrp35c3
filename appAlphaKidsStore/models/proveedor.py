from django.db import models


class Proveedor(models.Model):
    idproveedor = models.AutoField(primary_key=True)
    nombrerazonsocial = models.CharField(max_length=500, default='default value')
    direccion = models.CharField(max_length=500, default='default value')
    telefono = models.CharField(null=False, max_length=50, unique=True)
    contacto = models.CharField(max_length=100, default='default value')
    paginaweb = models.CharField(max_length=100, default='www') 
    fecharegistro = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        db_table = 'proveedor'