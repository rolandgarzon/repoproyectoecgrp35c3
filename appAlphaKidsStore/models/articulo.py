from django.db import models
from .marca import Marca
from .tipoarticulo import TipoArticulo
from .talla import Talla
from .color import Color
from .proveedor import Proveedor

class Articulo(models.Model):
    idarticulo = models.AutoField('Codigo',primary_key=True)
    referencia = models.CharField('Referencia',max_length=100, default='default value')
    descripcion = models.CharField('Descripcion', max_length=500, default='default value')
    idmarca = models.ForeignKey(Marca, related_name='articulo', on_delete=models.CASCADE)
    idtipoarticulo = models.ForeignKey(TipoArticulo, related_name='articulo', on_delete=models.CASCADE)
    idtalla = models.ForeignKey(Talla, related_name='articulo', on_delete=models.CASCADE)
    idcolor = models.ForeignKey(Color, related_name='articulo', on_delete=models.CASCADE)
    idproveedor = models.ForeignKey(Proveedor, related_name='articulo', on_delete=models.CASCADE)
    fecharegistro = models.DateTimeField('Fecha Registro',auto_now_add=True)
    urlfoto = models.CharField(max_length=100, default='default value')
    
    class Meta:
        db_table = 'articulo'
    
    
    