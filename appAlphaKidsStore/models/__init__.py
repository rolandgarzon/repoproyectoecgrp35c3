from .account import Account
from .user import User
from .articulo import Articulo
from .color import Color
from .marca import Marca
from .proveedor import Proveedor
from .talla import Talla
from .tipoarticulo import TipoArticulo