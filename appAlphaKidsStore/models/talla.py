from django.db import models

class Talla(models.Model):
    idtalla = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=500, default='default value')
    
    class Meta:
        db_table = 'talla'