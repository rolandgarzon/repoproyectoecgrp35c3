from django.db import models

class TipoArticulo(models.Model):
    idtipoarticulo = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=500, default='default value')
    
    class Meta:
        db_table = 'tipoarticulo'