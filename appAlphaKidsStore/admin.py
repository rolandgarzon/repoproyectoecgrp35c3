from django.contrib import admin

# Register your models here.
from .models.user import User
from .models.account import Account
from .models.proveedor import Proveedor
admin.site.register(User)
admin.site.register(Account)
