from appAlphaKidsStore.models.proveedor import Proveedor 
from rest_framework import serializers

class ProveedorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proveedor
        fields = ['idproveedor','nombrerazonsocial','direccion','telefono','contacto','paginaweb','fecharegistro']