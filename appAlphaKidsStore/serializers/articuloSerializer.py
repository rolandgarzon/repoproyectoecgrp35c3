from appAlphaKidsStore.models.articulo import Articulo
from rest_framework import serializers

class ArticuloSerializer(serializers.ModelSerializer):
    class Meta:
        model = Articulo
        fields = ['idarticulo', 'referencia', 'descripcion', 'idmarca', 'idtipoarticulo', 'idtalla','idcolor','idproveedor','fecharegistro','urlfoto']