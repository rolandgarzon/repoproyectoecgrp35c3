from django.apps import AppConfig


class AppalphakidsstoreConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'appAlphaKidsStore'
