asgiref==3.4.1
certifi==2021.10.8
charset-normalizer==2.0.6
dj-database-url==0.5.0
django==3.2.7
django-heroku==0.3.1
djangorestframework==3.12.4
djangorestframework-simplejwt==4.8.0
gunicorn==20.1.0
idna==3.2
psycopg2==2.9.1
PyJWT==2.1.0
python-dateutil==2.4.2
pytz==2021.1
requests==2.26.0
six==1.16.0
sqlparse==0.4.2
tldextract==1.6
unicodecsv==0.13.0
urllib3==1.26.7
whitenoise==5.3.0
django-cors-headers==3.9.0